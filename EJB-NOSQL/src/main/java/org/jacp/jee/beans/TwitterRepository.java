/*
 * Copyright (c) 2013, Andy Moncsek, inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 *    Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *    following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *    the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *    Neither the name of Andy Moncsek, inc. nor the names of its contributors may be used to endorse or
 *    promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jacp.jee.beans;

import org.jacp.jee.api.TwitterRepositoryRemote;
import org.jacp.jee.nosql.entity.TweetQuery;
import org.jacp.jee.nosql.entity.TwitterResult;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 18.03.13
 * Time: 21:30
 * To change this template use File | Settings | File Templates.
 */
@Stateless(mappedName = "TwitterRepository")
@Remote(TwitterRepositoryRemote.class)
public class TwitterRepository implements TwitterRepositoryRemote {
    @PersistenceContext(unitName = "mongo-example")
    private EntityManager em;

    @Override
    public List<TweetQuery> getAllAvailableTweetQueries() {
        TypedQuery<TweetQuery> query =
                em.createNamedQuery("TwitterResult.getAllTweetQueries", TweetQuery.class);
        return query.getResultList();
    }

    @Override
    public List<TwitterResult> getTwitterResultsByQuery(TweetQuery tQuery){
        TypedQuery<TwitterResult> query =
                em.createNamedQuery("TwitterResult.getAllByTweetQuery", TwitterResult.class);
        query.setParameter("query",query);
        return query.getResultList();
    }

    @Override
    public List<TwitterResult> getAllTwitterResults(){
        TypedQuery<TwitterResult> query =
                em.createNamedQuery("TwitterResult.getAll", TwitterResult.class);
        return query.getResultList();
    }

    @Override
    public void save(TwitterResult result) {
        em.persist(result);
    }
}
