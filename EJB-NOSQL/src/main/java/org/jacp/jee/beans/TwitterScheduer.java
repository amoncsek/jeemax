package org.jacp.jee.beans;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.jacp.jee.api.TwitterRepositoryRemote;
import org.jacp.jee.nosql.entity.TwitterResult;

import javax.ejb.*;
import javax.ejb.Timer;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 20.03.13
 * Time: 11:12
 * To change this template use File | Settings | File Templates.
 */
@Singleton(mappedName = "TwitterScheduer")
@Startup
@Local
@Named
public class TwitterScheduer {
    @EJB(mappedName = "TwitterRepository")
    private TwitterRepositoryRemote repo;
    List<String> list = new ArrayList<>(Arrays.asList("glassfish", "gf_jersey", "websockets", "java","javafx","oracle"));
    private Gson parser = new GsonBuilder()
            .setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").create();
    @Schedule(hour = "*", minute = "*", second = "*/2")
    public void sendTweets() {
        getFeedbackDataAndSave();

    }

    @Asynchronous
    public void getFeedbackDataAndSave() {
        for(String query : list) {
            String message = getFeedData(query);
            TwitterResult result=  parser.fromJson(message, TwitterResult.class);
            repo.save(result);
            System.out.println(message);
        }
    }




    private String getFeedData(String input) {
        StringBuilder sb = new StringBuilder();
        String searchURL = "http://search.twitter.com/search.json?q=" + input + "&rpp=5&include_entities=true" +
                "&with_twitter_user_id=true&result_type=mixed";
        try {
            URL twitter = new URL(searchURL);
            URLConnection yc;

            yc = twitter.openConnection();


            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            yc.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine);

            }
            in.close();

            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Error in getting data from the feeds ";
    }
}
