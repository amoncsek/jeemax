package org.jacp.chat.dialogs;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import org.jacp.api.annotations.Dialog;
import org.jacp.api.annotations.Resource;
import org.jacp.api.dialog.Scope;
import org.jacp.javafx.rcp.component.AFXComponent;
import org.jacp.javafx.rcp.components.modalDialog.JACPModalDialog;
import org.jacp.jee.messages.LoginMessage;

/**
 * Created with IntelliJ IDEA.
 * User: Andy moncsek
 * Date: 06.03.13
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */
@Dialog(id = "id100", viewLocation = "/fxml/LoginDialog.fxml", resourceBundleLocation = "bundles.languageBundle", localeID = "en_US", scope = Scope.SINGLETON)
public class LoginDialog {

    @FXML
    private TextField login;
    @Resource
    private AFXComponent parent;

    @FXML
    public void login() {
        if (login.getText() != null && !login.getText().isEmpty()) {
            parent.getActionListener("id003", new LoginMessage(login.getText())).performAction(null);
            parent.getActionListener("id002", new LoginMessage(login.getText())).performAction(null);
            JACPModalDialog.getInstance().hideModalMessage();
        }

    }
}
