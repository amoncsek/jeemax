package org.jacp.chat.encoders;

import org.apache.commons.lang.SerializationUtils;
import org.jacp.jee.messages.LoginMessage;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import java.nio.ByteBuffer;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 28.02.13
 * Time: 09:50
 * To change this template use File | Settings | File Templates.
 */
public class ClientLoginEncoder  implements Encoder.Binary<LoginMessage> {
    @Override
    public ByteBuffer encode(LoginMessage loginMessage) throws EncodeException {
        return ByteBuffer.wrap(SerializationUtils.serialize(loginMessage));
    }

    @Override
    public void init(EndpointConfig endpointConfig) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void destroy() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
