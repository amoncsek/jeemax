package org.jacp.chat.entities;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 06.03.13
 * Time: 11:48
 * To change this template use File | Settings | File Templates.
 */
public class User {
    private final String userName;

    public User(final String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return this.userName;
    }
}
