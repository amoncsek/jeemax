/************************************************************************
 *
 * Copyright (C) 2010 - 2012
 *
 * [WebSocketEndpoint.java]
 * AHCP Project (http://jacp.googlecode.com)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *     http://www.apache.org/licenses/LICENSE-2.0 
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either 
 * express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 *
 *
 ************************************************************************/
package org.jacp.chat.callbacks;

import javafx.event.Event;
import org.glassfish.tyrus.client.ClientManager;
import org.jacp.api.action.IAction;
import org.jacp.api.annotations.CallbackComponent;
import org.jacp.api.annotations.OnStart;
import org.jacp.api.annotations.OnTearDown;
import org.jacp.javafx.rcp.component.AStatefulCallbackComponent;
import org.jacp.jee.messages.ChatMessage;
import org.jacp.jee.messages.LoginMessage;
import org.jacp.jee.messages.Message;
import org.jacp.jee.messages.UserAddMessage;
import org.jacp.chat.decoders.MessageDecoder;
import org.jacp.chat.encoders.ChatMessageEncoder;
import org.jacp.chat.encoders.ClientLoginEncoder;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;


/**
 * A Stateful JacpFX component.
 *
 * @author <a href="mailto:amo.ahcp@gmail.com"> Andy Moncsek</a>
 */
@CallbackComponent(id = "id003", name = "statefulCallback", active = false)
@ClientEndpoint(encoders = {ClientLoginEncoder.class, ChatMessageEncoder.class}, decoders = {MessageDecoder.class})
public class WebSocketEndpoint extends AStatefulCallbackComponent {
    private Session session;
    private CountDownLatch messageLatch = new CountDownLatch(1);
    private final Logger log = Logger.getLogger(WebSocketEndpoint.class
            .getName());

    @Override
    public Object handleAction(final IAction<Event, Object> arg0) {
        if (arg0.getLastMessage() instanceof LoginMessage) {
            login((LoginMessage) arg0.getLastMessage());
        } else if (arg0.getLastMessage() instanceof ChatMessage) {
            sendMessage((ChatMessage) arg0.getLastMessage());
        }
        return null;
    }

    /**
     * send login to endpoint
     *
     * @param message
     */
    public void login(LoginMessage message) {
        try {
            session.getBasicRemote().sendObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (EncodeException e) {
            e.printStackTrace();
        }

    }

    /**
     * Send websocket message
     *
     * @param message
     */
    public void sendMessage(ChatMessage message) {
        try {
            session.getBasicRemote().sendObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (EncodeException e) {
            e.printStackTrace();
        }

    }

    @OnStart
    public void init() {
        this.log.info("WebSocketEndpoint start");
        ClientManager client = ClientManager.createClient();
        try {
            session = client.connectToServer(this, ClientEndpointConfig.Builder.create().build(), getURI());
            messageLatch.await(5, TimeUnit.SECONDS);
        } catch (DeploymentException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @OnOpen
    public void onOpen(Session session) {
        this.session = session;
        messageLatch.countDown();
    }

    @OnMessage
    public void onChatMessage(Message chatMessage) {
        switch (chatMessage.getType()){
            case MESSAGE:
                ChatMessage message = (ChatMessage) chatMessage;
                log.info("twitter message: " + message.getMessage());
                this.getActionListener("id002", message).performAction(null);
                break;
            case ADD_USER:
                UserAddMessage userAdd = (UserAddMessage) chatMessage;
                log.info("user added: " + userAdd.getUserName());
                this.getActionListener("id001", userAdd).performAction(null);
                break;
        }

    }


    @OnTearDown
    public void cleanup() {
        this.log.info("WebSocketEndpoint stop");
        try {
            session.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private URI getURI() {
        try {
            return new URI("ws", null, "localhost", 8080, "/WebClient-ChatWebsocket/chat", null, null);
            //  return new URI("ws", null, "localhost", 8080, "/WebClient-JMS-Websocket/jmschat", null, null);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

}
