/*
 * Copyright (c) 2013, Andy Moncsek, inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 *    Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *    following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *    the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *    Neither the name of Andy Moncsek, inc. nor the names of its contributors may be used to endorse or
 *    promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jacp.chat.components;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.jacp.api.action.IAction;
import org.jacp.api.annotations.DeclarativeComponent;
import org.jacp.api.annotations.OnStart;
import org.jacp.api.annotations.OnTearDown;
import org.jacp.javafx.rcp.component.AFXComponent;
import org.jacp.javafx.rcp.componentLayout.FXComponentLayout;
import org.jacp.javafx.rcp.components.managedDialog.JACPManagedDialog;
import org.jacp.javafx.rcp.components.managedDialog.ManagedDialogHandler;
import org.jacp.javafx.rcp.components.modalDialog.JACPModalDialog;
import org.jacp.javafx.rcp.util.FXUtil.MessageUtil;
import org.jacp.jee.messages.ChatMessage;
import org.jacp.jee.messages.LoginMessage;
import org.jacp.chat.dialogs.LoginDialog;
import org.jacp.chat.entities.User;

import java.text.DateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * A simple JacpFX FXML UI component
 *
 * @author <a href="mailto:amo.ahcp@gmail.com"> Andy Moncsek</a>
 */
@DeclarativeComponent(defaultExecutionTarget = "PMain", id = "id002", name = "componentRight", active = true, viewLocation = "/fxml/ComponentRightFXML.fxml", resourceBundleLocation = "bundles.languageBundle", localeID = "en_US")
public class ChatView extends AFXComponent {

    private final Logger log = Logger.getLogger(ChatView.class
            .getName());
    @FXML
    private TextField message;

    @FXML
    private TextArea descriptionValue;

    private User currentUser;

    @Override
    /**
     * The handleAction method always runs outside the main application thread. You can create new nodes, execute long running tasks but you are not allowed to manipulate existing nodes here.
     */
    public Node handleAction(final IAction<Event, Object> action) {
        if (action.getLastMessage() instanceof LoginMessage) {
            LoginMessage user = (LoginMessage) action.getLastMessage();
            currentUser = new User(user.getUserName());
        }
        return null;
    }

    @Override
    /**
     * The postHandleAction method runs always in the main application thread.
     */
    public Node postHandleAction(final Node arg0,
                                 final IAction<Event, Object> action) {
        // runs in FX application thread
        if (action.getLastMessage().equals(MessageUtil.INIT)) {
            ManagedDialogHandler<LoginDialog> handler = JACPManagedDialog.getInstance().getManagedDialog(
                    LoginDialog.class);
            JACPModalDialog.getInstance().showModalMessage(handler.getDialogNode());
        } else if (action.getLastMessage() instanceof ChatMessage) {
            ChatMessage message = (ChatMessage) action.getLastMessage();
            String current = descriptionValue.getText();
            String updatedText = current.concat("\n")
                    .concat(DateFormat.getTimeInstance(DateFormat.MEDIUM).format(new Date()))
                    .concat(" : ").concat(message.getUserName()).concat(": ").concat(message.getMessage());
            descriptionValue.setText(updatedText);
        }
        return null;
    }

    @OnStart
    /**
     * The @OnStart annotation labels methods executed when the component switch from inactive to active state
     * @param arg0
     * @param resourceBundle
     */
    public void onStartComponent(final FXComponentLayout arg0,
                                 final ResourceBundle resourceBundle) {
        this.log.info("run on start of ComponentRight ");

    }

    @OnTearDown
    /**
     * The @OnTearDown annotations labels methods executed when the component is set to inactive
     * @param arg0
     */
    public void onTearDownComponent(final FXComponentLayout arg0) {
        this.log.info("run on tear down of ComponentRight ");

    }


    @FXML
    private void handleSend(final ActionEvent event) {
        String messageText = message.getText();
        this.getActionListener("id003", new ChatMessage(currentUser.getUserName(), messageText))
                .performAction(null);
        message.setText("");
    }

}
