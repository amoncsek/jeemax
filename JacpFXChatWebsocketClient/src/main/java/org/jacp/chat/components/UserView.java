/************************************************************************
 *
 * Copyright (C) 2010 - 2012
 *
 * [UserView.java]
 * AHCP Project (http://jacp.googlecode.com)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *     http://www.apache.org/licenses/LICENSE-2.0 
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either 
 * express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 *
 *
 ************************************************************************/
package org.jacp.chat.components;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.AnchorPaneBuilder;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.util.Callback;
import org.jacp.api.action.IAction;
import org.jacp.api.annotations.Component;
import org.jacp.api.annotations.OnStart;
import org.jacp.api.annotations.OnTearDown;
import org.jacp.javafx.rcp.component.AFXComponent;
import org.jacp.javafx.rcp.componentLayout.FXComponentLayout;
import org.jacp.javafx.rcp.util.FXUtil.MessageUtil;
import org.jacp.jee.messages.UserAddMessage;
import org.jacp.chat.entities.User;

import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * A simple JacpFX UI component
 *
 * @author <a href="mailto:amo.ahcp@gmail.com"> Andy Moncsek</a>
 */
@Component(defaultExecutionTarget = "Pleft", id = "id001", name = "componentLeft", active = true, resourceBundleLocation = "bundles.languageBundle", localeID = "en_US")
public class UserView extends AFXComponent {
    private AnchorPane pane;
    private ObservableList<User> users = FXCollections.observableArrayList();
    private final Logger log = Logger.getLogger(UserView.class.getName());


    @Override
    /**
     * The handleAction method always runs outside the main application thread. You can create new nodes, execute long running tasks but you are not allowed to manipulate existing nodes here.
     */
    public Node handleAction(final IAction<Event, Object> action) {
        // runs in worker thread
        if (action.getLastMessage().equals(MessageUtil.INIT)) {
            return this.createUI();
        } else if (action.getLastMessage() instanceof UserAddMessage) {
            UserAddMessage user = (UserAddMessage) action.getLastMessage();
            users.add(new User(user.getUserName()));
        }
        return null;
    }

    @Override
    /**
     * The postHandleAction method runs always in the main application thread.
     */
    public Node postHandleAction(final Node arg0,
                                 final IAction<Event, Object> action) {
        // runs in FX application thread
        if (action.getLastMessage().equals(MessageUtil.INIT)) {
            this.pane = (AnchorPane) arg0;
        }
        return this.pane;
    }

    @OnStart
    /**
     * The @OnStart annotation labels methods executed when the component switch from inactive to active state
     * @param arg0
     */
    public void onStartComponent(final FXComponentLayout arg0,
                                 final ResourceBundle resourceBundle) {
        this.log.info("run on start of UserView ");
    }

    @OnTearDown
    /**
     * The @OnTearDown annotations labels methods executed when the component is set to inactive
     * @param arg0
     */
    public void onTearDownComponent(final FXComponentLayout arg0) {
        this.log.info("run on tear down of UserView ");

    }

    /**
     * create the UI on first call
     *
     * @return
     */
    private Node createUI() {
        final AnchorPane anchor = AnchorPaneBuilder.create()
                .styleClass("roundedAnchorPaneFX").build();
        final Label heading = LabelBuilder.create()
                .text(this.getResourceBundle().getString("registeredUsers"))
                .alignment(Pos.CENTER_RIGHT).styleClass("propLabelBig").build();

        final TreeItem<User> rootNode =
                new TreeItem<User>(new User("----"));
        final TreeView<User> treeView = new TreeView<User>(rootNode);
        users.addListener(new ListChangeListener<User>() {
            @Override
            public void onChanged(Change<? extends User> change) {
                rootNode.getChildren().clear();
                if (change == null) return;
                for (User u : change.getList()) {
                    rootNode.getChildren().add(new TreeItem<>(u));
                }
            }
        });
        treeView.setShowRoot(false);
        treeView.setCellFactory(new Callback<TreeView<User>, TreeCell<User>>() {
            @Override
            public TreeCell<User> call(TreeView<User> userTreeView) {
                return new TreeCell<User>() {
                    @Override
                    public void updateItem(User item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            setText(item.getUserName());
                            setGraphic(getTreeItem().getGraphic());
                        }
                    }
                };
            }
        });
        AnchorPane.setRightAnchor(heading, 40.0);
        AnchorPane.setTopAnchor(heading, 15.0);


        AnchorPane.setTopAnchor(treeView, 50.0);
        AnchorPane.setRightAnchor(treeView, 25.0);
        AnchorPane.setLeftAnchor(treeView, 25.0);

        anchor.getChildren().addAll(heading, treeView);

        GridPane.setHgrow(anchor, Priority.ALWAYS);
        GridPane.setVgrow(anchor, Priority.ALWAYS);

        return anchor;
    }

}
