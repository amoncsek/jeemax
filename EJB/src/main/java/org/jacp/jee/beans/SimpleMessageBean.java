package org.jacp.jee.beans;

import org.jacp.jee.api.MessageEJBInterface;
import org.jacp.jee.entity.ClientMessage;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Stateless(mappedName ="SimpleMessageBean" )
@Remote(MessageEJBInterface.class)
public class SimpleMessageBean implements MessageEJBInterface {
    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    public void init() {
        System.out.println("crete SimpleMessageBean");
    }


    @PreDestroy
    public void close() {
        System.out.println("close SimpleMessageBean");
    }


    @Override
    public void create(ClientMessage message) {
        //em.persist(message);

    }

    @Override
    public List<ClientMessage> findAll() {
//	CriteriaQuery<ClientMessage> query = em.getCriteriaBuilder()
//		.createQuery(ClientMessage.class);
//	query.select(query.from(ClientMessage.class));
//
//	return em.createQuery(query).getResultList();
        List<ClientMessage> result = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            result.add(new ClientMessage("hallo" + i, "welt: " + i));
        }
        return result;
    }

    @Override
    public void call() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

}
