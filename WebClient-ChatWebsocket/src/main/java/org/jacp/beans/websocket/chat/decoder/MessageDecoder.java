package org.jacp.beans.websocket.chat.decoder;

import org.apache.commons.lang.SerializationUtils;
import org.jacp.jee.messages.Message;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;
import java.nio.ByteBuffer;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 27.02.13
 * Time: 15:32
 * To change this template use File | Settings | File Templates.
 */
public class MessageDecoder implements Decoder.Binary<Message> {
    @Override
    public Message decode(ByteBuffer byteBuffer) throws DecodeException {
        return (Message) SerializationUtils.deserialize(byteBuffer.array());  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean willDecode(ByteBuffer byteBuffer) {
        Object message = SerializationUtils.deserialize(byteBuffer.array());
        if (message == null) return false;
        return message instanceof Message;
    }

    @Override
    public void init(EndpointConfig endpointConfig) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void destroy() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}


