package org.jacp.beans.websocket.chat;


import org.jacp.beans.ejb.ChatUserBean;
import org.jacp.beans.websocket.chat.decoder.MessageDecoder;
import org.jacp.beans.websocket.chat.encoder.ChatMessageEncoder;
import org.jacp.beans.websocket.chat.encoder.UserAddMessageEncoder;
import org.jacp.jee.entity.ChatUser;
import org.jacp.jee.messages.ChatMessage;
import org.jacp.jee.messages.LoginMessage;
import org.jacp.jee.messages.Message;
import org.jacp.jee.messages.UserAddMessage;

import javax.inject.Inject;
import javax.websocket.EncodeException;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Andy Moncsek
 * Date: 26.02.13
 * Time: 15:57
 * To change this template use File | Settings | File Templates.
 */
@ServerEndpoint(value = "/chat", decoders = {
        MessageDecoder.class
},
        encoders = {
                UserAddMessageEncoder.class,
                ChatMessageEncoder.class, UserAddMessageEncoder.class
        })
public class ChatEndpoint {
    final static Logger logger = Logger.getLogger("application");

    @Inject
    private ChatUserBean chatBean;

    @OnOpen
    public void init(Session s) {
        logger.info("############Someone connected..." + s.getId() + "  socket: " + chatBean);
    }



    @OnMessage
    public boolean handleChatMessage(Message cm, Session session) {
        switch (cm.getType()){
            case MESSAGE:
                logger.info("############message received");
                broadcastMessage((ChatMessage) cm, session);
                break;
            case LOGIN:
                logger.info("############login received");
                LoginMessage lrm = (LoginMessage) cm;
                chatBean.saveNewUser(new ChatUser(lrm.getUserName(),session.getId()));
                broadcastNewUser(lrm.getUserName(), session);
                break;
        }
        return true;
    }

    private void broadcastMessage(ChatMessage cm, Session session) {
        for (Session s : session.getOpenSessions()) {
            logger.info("send message : " + cm.getMessage());
            try {
                s.getBasicRemote().sendObject(cm);
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (EncodeException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

        }
    }

    private void broadcastNewUser(String newUsername, Session session) {
        try {
            final List<ChatUser> all = chatBean.getAllUsers();
            for (Session s : session.getOpenSessions()) {
                if (s.getId().equals(session.getId())) {
                    for(ChatUser user : all){
                        session.getBasicRemote().sendObject(new UserAddMessage(user.getName()));
                    }
                }else {
                    s.getBasicRemote().sendObject(new UserAddMessage(newUsername));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (EncodeException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
