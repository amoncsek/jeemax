package org.jacp.beans.websocket.chat.encoder;

import org.apache.commons.lang.SerializationUtils;
import org.jacp.jee.messages.ChatMessage;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import java.nio.ByteBuffer;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 06.03.13
 * Time: 09:40
 * To change this template use File | Settings | File Templates.
 */
public class ChatMessageEncoder implements Encoder.Binary<ChatMessage> {
    @Override
    public ByteBuffer encode(ChatMessage message) throws EncodeException {
        return ByteBuffer.wrap(SerializationUtils.serialize(message));
    }

    @Override
    public void init(EndpointConfig endpointConfig) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void destroy() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}