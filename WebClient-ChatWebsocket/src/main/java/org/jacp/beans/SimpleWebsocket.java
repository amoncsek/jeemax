package org.jacp.beans;

import org.jacp.jee.api.MessageEJBInterface;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.websocket.MessageHandler;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;


@ServerEndpoint(value = "/echo")
@Singleton
public class SimpleWebsocket {
    @EJB(name = "SimpleMessageBean", beanName = "SimpleMessageBean")
    private MessageEJBInterface bean;

    @PostConstruct
    public void init() {
        System.out.println("crete SimpleWebsocket");
    }

    @OnOpen
    public void initSession(final Session session){
               /*session.addMessageHandler(new MessageHandler.Whole<String>() {
                   @Override
                   public void onMessage(String message) {
                       System.out.println("hallo from handler" + session + " :: " + this + "::: bean::  " + bean);
                       StringBuffer buffer = new StringBuffer("sessions: ");
                       Set<Session> sessions = session.getOpenSessions();
                       for (Session s : sessions) {
                           buffer.append(s.hashCode()).append(",");
                           if (!s.getId().equals(session.getId())) try {
                               s.getBasicRemote().sendText("hello from an other session: " + message+" current users:"+sessions.size()+" "+session.getRequestURI().toString() );
                           } catch (IOException e) {
                               e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                           }
                       }

                       String returnVal = "<ul>";
                       returnVal += "</ul>:::";

                       try {
                           session.getBasicRemote().sendText(buffer.toString() + " :::: " + returnVal);
                       } catch (IOException e) {
                           e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                       }


                   }
               });*/
    }

    @OnMessage
    public String sayHello(String name, Session session) throws IOException {
        System.out.println("hallo" + session + " :: " + this + "::: bean::  " + bean);
        Set<Session> sessions = session.getOpenSessions();

        StringBuffer buffer = new StringBuffer("sessions: ");
        for (Session s : sessions) {
            buffer.append(s.hashCode()).append(",");
            if (!s.getId().equals(session.getId())) s.getBasicRemote().sendText("hello from an other session: " + name+" current users:"+sessions.size()+" "+session.getRequestURI().toString() );
        }

        String returnVal = "<ul>";

        returnVal += "</ul>:::";
        return buffer.toString() + " :::: " + returnVal;
    }
}
