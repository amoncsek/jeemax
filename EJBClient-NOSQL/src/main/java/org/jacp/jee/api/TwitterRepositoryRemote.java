package org.jacp.jee.api;

import org.jacp.jee.nosql.entity.TweetQuery;
import org.jacp.jee.nosql.entity.TwitterResult;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 20.03.13
 * Time: 10:57
 * To change this template use File | Settings | File Templates.
 */
public interface TwitterRepositoryRemote {
    List<TweetQuery> getAllAvailableTweetQueries();

    List<TwitterResult> getTwitterResultsByQuery(TweetQuery tQuery);

    List<TwitterResult> getAllTwitterResults();

    void save(TwitterResult result);
}
