/************************************************************************
 *
 * Copyright (C) 2010 - 2012
 *
 * [TwitterEndpointComponent.java]
 * AHCP Project (http://jacp.googlecode.com)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *     http://www.apache.org/licenses/LICENSE-2.0 
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either 
 * express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 *
 *
 ************************************************************************/
package org.jacp.twitter.callbacks;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javafx.event.Event;
import org.glassfish.tyrus.client.ClientManager;
import org.jacp.api.action.IAction;
import org.jacp.api.annotations.CallbackComponent;
import org.jacp.api.annotations.OnStart;
import org.jacp.api.annotations.OnTearDown;
import org.jacp.javafx.rcp.component.AStatefulCallbackComponent;
import org.jacp.jee.entity.Query;
import org.jacp.jee.entity.TwitterResult;
import org.jacp.twitter.decoder.TwitterResultDecoder;
import org.jacp.twitter.encoder.QueryEncoder;


import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;


/**
 * A Stateful JacpFX component which acts as a WebSocket ClientEndpoint. It receives queries from UI and sends them to Server and it can receive TwitterResults from Server and pass them to UI
 *
 * @author <a href="mailto:amo.ahcp@gmail.com"> Andy Moncsek</a>
 */
@ClientEndpoint(decoders = {TwitterResultDecoder.class}, encoders = {QueryEncoder.class})
@CallbackComponent(id = "id003", name = "twitterEndpointComponent", active = false)
public class TwitterEndpointComponent extends AStatefulCallbackComponent {
    private Session session;
    private CountDownLatch messageLatch = new CountDownLatch(1);
    private final Logger log = Logger.getLogger(TwitterEndpointComponent.class
            .getName());

    @Override
    public Object handleAction(final IAction<Event, Object> arg0) {
        if (arg0.getLastMessage() instanceof Query) try {
            sendQuery((Query) arg0.getLastMessage());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (EncodeException e) {
            e.printStackTrace();
        }
        return null;
    }


    @OnStart
    /**
     * JacpFX lifecycle
     */
    public void init() {
        this.log.info("WebSocketEndpoint start");
        ClientManager client = ClientManager.createClient();
        try {
            session = client.connectToServer(this, ClientEndpointConfig.Builder.create().build(), getURI());
            messageLatch.await(5, TimeUnit.SECONDS);
        } catch (DeploymentException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @OnTearDown
    /**
     * JacpFX lifecycle
     */
    public void cleanup() {
        this.log.info("WebSocketEndpoint stop");
        try {
            if(session!=null)
                session.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnOpen
    /**
     * WebSocket lifecycle
     */
    public void onOpen(Session session) {
        this.session = session;
        messageLatch.countDown();
    }

    @OnMessage
    /**
     * WebSocket lifecycle
     */
    public void onTwitterMessage(TwitterResult result) {
        this.getActionListener("id001", result).performAction(null);
    }

    private void sendQuery(Query query) throws IOException, EncodeException {
        session.getBasicRemote().sendObject(query);
    }


    private URI getURI() {
        try {
            return new URI("ws", null, "localhost", 8080, "/WebClient-TwitterWebsocket/twitter", null, null);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }


}
