package org.jacp.jee.api;

import org.jacp.jee.entity.ClientMessage;

import javax.ejb.Remote;
import java.util.List;

/**
 * The remote interface
 *
 * @author Andy Moncsek
 */
@Remote
public interface MessageEJBInterface {
    void create(ClientMessage message);

    List<ClientMessage> findAll();

    void call();

}
