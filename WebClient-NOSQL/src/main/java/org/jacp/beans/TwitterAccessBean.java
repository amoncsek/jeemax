package org.jacp.beans;

import org.jacp.jee.api.TwitterRepositoryRemote;
import org.jacp.jee.nosql.entity.TweetQuery;
import org.jacp.jee.nosql.entity.TwitterResult;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 20.03.13
 * Time: 16:06
 * To change this template use File | Settings | File Templates.
 */
@Stateless(mappedName = "TwitterAccessBean")
public class TwitterAccessBean {
    @EJB(mappedName = "TwitterRepository")
    private TwitterRepositoryRemote repo;

    public List<TweetQuery> getAllAvailableTweetQueries(){
        return repo.getAllAvailableTweetQueries();
    }

    public List<TwitterResult> getTwitterResultsByQuery(TweetQuery tQuery){
        return repo.getTwitterResultsByQuery(tQuery);
    }

    public List<TwitterResult> getAllTwitterResults(){
        return repo.getAllTwitterResults();
    }
}
