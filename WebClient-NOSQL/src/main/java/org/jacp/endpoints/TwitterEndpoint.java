/*
 * Copyright (c) 2013, Andy Moncsek, inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 *    Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *    following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *    the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *    Neither the name of Andy Moncsek, inc. nor the names of its contributors may be used to endorse or
 *    promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jacp.endpoints;

/**
 * Created with IntelliJ IDEA.
 * User: Andy Moncsek
 * Date: 20.03.13
 * Time: 21:26
 * To change this template use File | Settings | File Templates.
 */

import org.jacp.beans.TwitterAccessBean;
import org.jacp.jee.nosql.entity.TweetQuery;

import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.List;
import java.util.logging.Logger;

@ServerEndpoint(value = "/twitterEndpoint")
public class TwitterEndpoint {
    final static Logger logger = Logger.getLogger(TwitterEndpoint.class.getName());
    @Inject
    private TwitterAccessBean repo;

    @OnOpen
    public void init(Session s) {
        logger.info("############Someone connected..." + s.getId() + "  socket: " + repo);
        List<TweetQuery> result = repo.getAllAvailableTweetQueries();
        for(TweetQuery q: result) {
            logger.info("Tweet: "+q.getQuery());
        }
    }
}
