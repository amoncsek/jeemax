package org.jacp.jee.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

//@Entity
public class ClientMessage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -6569693529124061369L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;
    private Serializable body;

    /**
     * Satisfy JPA provider.
     */
    protected ClientMessage() {
    }

    public ClientMessage(String title, Serializable body) {
        this.title = title;
        this.body = body;
    }

    public Serializable getBody() {
        return body;
    }

    public void setBody(Serializable body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
