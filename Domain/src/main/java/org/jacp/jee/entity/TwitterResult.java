package org.jacp.jee.entity;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Andy Moncsek
 * Date: 11.03.13
 * Time: 09:13
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement
public class TwitterResult implements Serializable{

    private String query;
    private String max_id;
    private List<Tweet> results;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getMax_id() {
        return max_id;
    }

    public void setMax_id(String max_id) {
        this.max_id = max_id;
    }

    public List<Tweet> getResults() {
        return results;
    }

    public void setResults(List<Tweet> results) {
        this.results = results;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TwitterResult that = (TwitterResult) o;

        if (max_id != null ? !max_id.equals(that.max_id) : that.max_id != null) return false;
        if (query != null ? !query.equals(that.query) : that.query != null) return false;
        return !(results != null ? !results.equals(that.results) : that.results != null);

    }

    @Override
    public int hashCode() {
        int result = query != null ? query.hashCode() : 0;
        result = 31 * result + (max_id != null ? max_id.hashCode() : 0);
        result = 31 * result + (results != null ? results.hashCode() : 0);
        return result;
    }
}
