package org.jacp.jee.constants;

public class Constants {
    /**
     * Name of the persistence unit as referenced in the persistence.xml
     * deployment descriptor file.
     */
    public static final String PU = "messagePU";
}
