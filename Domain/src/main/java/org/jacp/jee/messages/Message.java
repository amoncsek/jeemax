package org.jacp.jee.messages;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 27.02.13
 * Time: 09:16
 * To change this template use File | Settings | File Templates.
 */
public class Message implements Serializable {

    private final Map<String, Serializable> values = new HashMap<>();
    private final MessageType type;

    private String sourceSessionId;

    public Message(MessageType type) {
        this.type = type;
    }

    public MessageType getType() {
        return type;
    }

    public void addValue(String key, Serializable value) {
        this.values.put(key, value);
    }

    public Object getValue(String key) {
        return this.values.get(key);
    }

    public String getSourceSessionId() {
        return sourceSessionId;
    }

    public void setSourceSessionId(String sourceSessionId) {
        this.sourceSessionId = sourceSessionId;
    }
}
