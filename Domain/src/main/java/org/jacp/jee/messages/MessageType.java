package org.jacp.jee.messages;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 27.02.13
 * Time: 10:54
 * To change this template use File | Settings | File Templates.
 */
public enum MessageType {
    LOGIN, LOGOUT, MESSAGE, ADD_USER;
}
