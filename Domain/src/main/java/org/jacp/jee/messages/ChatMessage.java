package org.jacp.jee.messages;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 27.02.13
 * Time: 12:41
 * To change this template use File | Settings | File Templates.
 */
public class ChatMessage extends Message {

    private static String USERNAME_PROPERTY = "USER";
    private static String MESSAGE_PROPERTY = "MESSAGE";

    public ChatMessage(String userName, String message) {
        super(MessageType.MESSAGE);
        super.addValue(USERNAME_PROPERTY, userName);
        super.addValue(MESSAGE_PROPERTY, message);
    }

    public String getUserName() {
        return String.class.cast(super.getValue(USERNAME_PROPERTY));
    }

    public String getMessage() {
        return String.class.cast(super.getValue(MESSAGE_PROPERTY));
    }
}
