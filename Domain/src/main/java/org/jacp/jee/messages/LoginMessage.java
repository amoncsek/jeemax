package org.jacp.jee.messages;

/**
 * Created with IntelliJ IDEA.
 * User: Andy Moncsek
 * Date: 27.02.13
 * Time: 09:13
 * To change this template use File | Settings | File Templates.
 */
public class LoginMessage extends Message {

    private static String USERNAME_PROPERTY = "USER";

    public LoginMessage(String userName) {
        super(MessageType.LOGIN);
        super.addValue(USERNAME_PROPERTY, userName);
    }

    public String getUserName() {
        return String.class.cast(super.getValue(USERNAME_PROPERTY));
    }

}