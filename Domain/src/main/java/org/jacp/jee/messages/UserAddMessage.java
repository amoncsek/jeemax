package org.jacp.jee.messages;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 27.02.13
 * Time: 10:15
 * To change this template use File | Settings | File Templates.
 */
public class UserAddMessage extends Message {
    private static String USERNAME_PROPERTY = "USER";

    public UserAddMessage() {
        super(MessageType.ADD_USER);
    }

    public UserAddMessage(String username) {
        super(MessageType.ADD_USER);
        super.addValue(USERNAME_PROPERTY, username);
    }

    public String getUserName() {
        return String.class.cast(super.getValue(USERNAME_PROPERTY));
    }

}
