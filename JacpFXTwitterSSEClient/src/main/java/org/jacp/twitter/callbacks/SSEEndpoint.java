/************************************************************************
 *
 * Copyright (C) 2010 - 2012
 *
 * [SSEEndpoint.java]
 * AHCP Project (http://jacp.googlecode.com)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *     http://www.apache.org/licenses/LICENSE-2.0 
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either 
 * express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 *
 *
 ************************************************************************/
package org.jacp.twitter.callbacks;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javafx.event.Event;
import org.glassfish.jersey.SslConfigurator;
import org.glassfish.jersey.media.sse.EventListener;
import org.glassfish.jersey.media.sse.EventSource;
import org.glassfish.jersey.media.sse.InboundEvent;
import org.jacp.api.action.IAction;
import org.jacp.api.annotations.CallbackComponent;
import org.jacp.api.annotations.OnStart;
import org.jacp.api.annotations.OnTearDown;
import org.jacp.javafx.rcp.component.AStatefulCallbackComponent;
import org.jacp.jee.entity.TwitterResult;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;


/**
 * A Stateful JacpFX component.
 *
 * @author <a href="mailto:amo.ahcp@gmail.com"> Andy Moncsek</a>
 */
@CallbackComponent(id = "id003", name = "statefulCallback", active = true)
public class SSEEndpoint extends AStatefulCallbackComponent {
    private Client client;
    private EventSource eventSource;
    private final Logger log = Logger.getLogger(SSEEndpoint.class
            .getName());

    @Override
    public Object handleAction(final IAction<Event, Object> arg0) {
        return null;
    }



    @OnStart
    public void init() {
        this.log.info("SSEEndpoint start");
        SslConfigurator sslConfig = SslConfigurator.newInstance()
                .trustStoreFile("NONE")
                .keyStoreFile("NONE");

        client = ClientBuilder.newBuilder().sslContext(sslConfig.createSSLContext()).build();
        final javax.ws.rs.client.WebTarget webTarget = client.target(getURI());
        eventSource = new EventSource(webTarget) {
            @Override
            public void onEvent(InboundEvent inboundEvent) {
                //get the JSON data and parse it
                Gson parser = new GsonBuilder()
                        .setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").create();
                try {
                    log.info("got message");
                    TwitterResult result=  parser.fromJson(inboundEvent.getData(String.class,
                                MediaType.APPLICATION_JSON_TYPE), TwitterResult.class);
                    log.info("send message");
                    getActionListener("id001",result).performAction(null);
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

            }
        };
    }




    @OnTearDown
    public void cleanup() {
        eventSource.close();
        client.close();
        this.log.info("SSEEndpoint stop");

    }

    private URI getURI() {
        try {
           return new URI("http", null, "localhost", 8080, "/WebClient-TwitterSSE/twittersse", null, null);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

}
