/************************************************************************
 *
 * Copyright (C) 2010 - 2012
 *
 * [ComponentRight.java]
 * AHCP Project (http://jacp.googlecode.com)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *     http://www.apache.org/licenses/LICENSE-2.0 
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either 
 * express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 *
 *
 ************************************************************************/
package org.jacp.twitter.components;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import org.glassfish.jersey.SslConfigurator;
import org.jacp.api.action.IAction;
import org.jacp.api.annotations.Component;
import org.jacp.api.annotations.OnStart;
import org.jacp.api.annotations.OnTearDown;
import org.jacp.javafx.rcp.component.AFXComponent;
import org.jacp.javafx.rcp.componentLayout.FXComponentLayout;
import org.jacp.javafx.rcp.util.FXUtil.MessageUtil;
import org.jacp.twitter.entities.Clear;
import org.jacp.jee.entity.Query;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * A simple JacpFX FXML UI component
 *
 * @author <a href="mailto:amo.ahcp@gmail.com"> Andy Moncsek</a>
 */
@Component(defaultExecutionTarget = "Ptop", id = "id002", name = "componentRight", active = true, resourceBundleLocation = "bundles.languageBundle", localeID = "en_US")
public class QueryView extends AFXComponent {
    private final static String TARGET_URI = "http://localhost:8080/WebClient-TwitterSSE";
    private final Logger log = Logger.getLogger(QueryView.class
            .getName());


    @Override
    /**
     * The handleAction method always runs outside the main application thread. You can create new nodes, execute long running tasks but you are not allowed to manipulate existing nodes here.
     */
    public Node handleAction(final IAction<Event, Object> action) {
           if(action.getLastMessage() instanceof Query) {
               Query query = (Query) action.getLastMessage();
               SslConfigurator sslConfig = SslConfigurator.newInstance()
                       .trustStoreFile("NONE")
                       .keyStoreFile("NONE");

               Client client = ClientBuilder.newBuilder().sslContext(sslConfig.createSSLContext()).build();
               WebTarget webTarget;
               try {
                   webTarget = client.target(new URI(TARGET_URI));
                   webTarget.path("query").request().post(Entity.text(query.getQuery()));
               } catch (URISyntaxException e) {
                   e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
               }

           }
        return null;
    }

    @Override
    /**
     * The postHandleAction method runs always in the main application thread.
     */
    public Node postHandleAction(final Node arg0,
                                 final IAction<Event, Object> action) {
        // runs in FX application thread
        if (action.getLastMessage().equals(MessageUtil.INIT)) {
            return createUI();
        }
        return null;
    }

    private Node createUI() {
        final AnchorPane anchor = AnchorPaneBuilder.create()
                .styleClass("roundedAnchorPaneFX").build();

        HBox hbox = new HBox();
        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setSpacing(15);
        final Label label = new Label("TwitterQuery");
        final TextField textField = new TextField();
        textField.setMinWidth(100);
        final Button button = new Button("OK");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                getActionListener(new Query(textField.getText())).performAction(actionEvent);
                getActionListener("id001",new Clear()).performAction(actionEvent);

            }
        });
        hbox.getChildren().addAll(label,textField,button);

        AnchorPane.setTopAnchor(hbox, 25.0);
        AnchorPane.setRightAnchor(hbox, 25.0);
        AnchorPane.setLeftAnchor(hbox, 25.0);

        anchor.getChildren().addAll(hbox);

        GridPane.setHgrow(anchor, Priority.ALWAYS);
        GridPane.setVgrow(anchor, Priority.ALWAYS);

        return anchor;
    }

    @OnStart
    /**
     * The @OnStart annotation labels methods executed when the component switch from inactive to active state
     * @param arg0
     * @param resourceBundle
     */
    public void onStartComponent(final FXComponentLayout arg0,
                                 final ResourceBundle resourceBundle) {
        this.log.info("run on start of ComponentRight ");

    }

    @OnTearDown
    /**
     * The @OnTearDown annotations labels methods executed when the component is set to inactive
     * @param arg0
     */
    public void onTearDownComponent(final FXComponentLayout arg0) {
        this.log.info("run on tear down of ComponentRight ");

    }



}
