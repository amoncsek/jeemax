/************************************************************************
 *
 * Copyright (C) 2010 - 2012
 *
 * [TweetView.java]
 * AHCP Project (http://jacp.googlecode.com)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *     http://www.apache.org/licenses/LICENSE-2.0 
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either 
 * express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 *
 *
 ************************************************************************/
package org.jacp.twitter.components;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.scene.Node;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.util.Callback;
import org.jacp.api.action.IAction;
import org.jacp.api.annotations.Component;
import org.jacp.api.annotations.OnStart;
import org.jacp.api.annotations.OnTearDown;
import org.jacp.javafx.rcp.component.AFXComponent;
import org.jacp.javafx.rcp.componentLayout.FXComponentLayout;
import org.jacp.javafx.rcp.util.FXUtil.MessageUtil;
import org.jacp.jee.entity.Tweet;
import org.jacp.jee.entity.TwitterResult;
import org.jacp.twitter.entities.Clear;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * A simple JacpFX UI component
 *
 * @author <a href="mailto:amo.ahcp@gmail.com"> Andy Moncsek</a>
 */
@Component(defaultExecutionTarget = "PMain", id = "id001", name = "componentLeft", active = true, resourceBundleLocation = "bundles.languageBundle", localeID = "en_US")
public class TweetView extends AFXComponent {
    private AnchorPane pane;
    private ObservableList<Tweet> tweets = FXCollections.observableArrayList();
    private final Logger log = Logger.getLogger(TweetView.class.getName());


    @Override
    /**
     * The handleAction method always runs outside the main application thread. You can create new nodes, execute long running tasks but you are not allowed to manipulate existing nodes here.
     */
    public Node handleAction(final IAction<Event, Object> action) {
        // runs in worker thread
        if (action.getLastMessage().equals(MessageUtil.INIT)) {
            return this.createUI();
        }
        return null;
    }

    @Override
    /**
     * The postHandleAction method runs always in the main application thread.
     */
    public Node postHandleAction(final Node arg0,
                                 final IAction<Event, Object> action) {
        // runs in FX application thread
        if (action.getLastMessage().equals(MessageUtil.INIT)) {
            this.pane = (AnchorPane) arg0;
        } else if (action.getLastMessage() instanceof TwitterResult) {
            TwitterResult result = (TwitterResult) action.getLastMessage();
            if (!result.getResults().isEmpty()) {
                tweets.addAll(result.getResults());
                Collections.sort(tweets);
            }
        } else if(action.getLastMessage() instanceof Clear){
            tweets.clear();
        }
        return this.pane;
    }

    @OnStart
    /**
     * The @OnStart annotation labels methods executed when the component switch from inactive to active state
     * @param arg0
     */
    public void onStartComponent(final FXComponentLayout arg0,
                                 final ResourceBundle resourceBundle) {
        this.log.info("run on start of TweetView ");
    }

    @OnTearDown
    /**
     * The @OnTearDown annotations labels methods executed when the component is set to inactive
     * @param arg0
     */
    public void onTearDownComponent(final FXComponentLayout arg0) {
        this.log.info("run on tear down of TweetView ");

    }

    /**
     * create the UI on first call
     *
     * @return
     */
    private Node createUI() {
        final AnchorPane anchor = AnchorPaneBuilder.create()
                .styleClass("roundedAnchorPaneFX").build();

        TableView table = new TableView();
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.getColumns().addAll(createColumns());
        table.setItems(tweets);

        AnchorPane.setTopAnchor(table, 25.0);
        AnchorPane.setRightAnchor(table, 25.0);
        AnchorPane.setLeftAnchor(table, 25.0);

        anchor.getChildren().addAll(table);

        GridPane.setHgrow(anchor, Priority.ALWAYS);
        GridPane.setVgrow(anchor, Priority.ALWAYS);

        return anchor;
    }

    private List<TableColumn> createColumns() {
        TableColumn imageView = new TableColumn("image");
        imageView.setMinWidth(60);
        imageView.setCellValueFactory(
                new PropertyValueFactory<Tweet, String>("profile_image_url"));
        imageView.setCellFactory(new Callback<TableColumn<Tweet, String>, TableCell<Tweet, String>>() {

            @Override
            public TableCell<Tweet, String> call(TableColumn<Tweet, String> tweetStringTableColumn) {
                return new TableCell<Tweet, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        if (item!=null) {
                            HBox box = new HBox();
                            box.setSpacing(10);
                            VBox vbox = new VBox();
                            ImageView imageview = new ImageView();
                            imageview.setFitHeight(50);
                            imageview.setFitWidth(50);
                            imageview.setImage(new Image(item, true));
                            box.getChildren().addAll(imageview, vbox);
                            setGraphic(box);
                        }
                    }
                };
                  //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        TableColumn nameView = new TableColumn("name");
        nameView.setMinWidth(100);
        nameView.setCellValueFactory(
                new PropertyValueFactory<Tweet, String>("from_user"));
        TableColumn messageView = new TableColumn("message");
        messageView.setMinWidth(500);
        messageView.setCellValueFactory(
                new PropertyValueFactory<Tweet, String>("text"));
        return Arrays.asList(imageView, nameView, messageView);
    }

}
