/*
 * Copyright (c) 2013, Andy Moncsek, inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 *    Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *    following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *    the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *    Neither the name of Andy Moncsek, inc. nor the names of its contributors may be used to endorse or
 *    promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jacp.sse.beans;

import org.glassfish.jersey.SslConfigurator;
import org.jacp.sse.SSEResource;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.ejb.Timer;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Andy Moncsek
 * Date: 07.03.13
 * Time: 16:06
 * To change this template use File | Settings | File Templates.
 */
@Singleton(mappedName = "TwitterTimerBean")
@Startup
@Local
@Named
public class TwitterTimerBean {

    private final static String TARGET_URI = "http://localhost:8080/WebClient-TwitterSSE";
    List<String> list = new ArrayList<>(Arrays.asList("glassfish","gf_jersey","websockets","java"));
    @Resource
    TimerService timerService;

    @PostConstruct
    public void init(){
        ScheduleExpression schedule = new ScheduleExpression();
        schedule.minute("*");
        schedule.hour("*");
        schedule.second("*/10");
        timerService.createCalendarTimer(schedule);
    }

    //@Schedule(hour = "*", minute = "*", second = "*/10")
    @Timeout
    public void sendTweets() {
        SslConfigurator sslConfig = SslConfigurator.newInstance()
                .trustStoreFile("NONE")
                .keyStoreFile("NONE");

        Client client = ClientBuilder.newBuilder().sslContext(sslConfig.createSSLContext()).build();
        try {
            WebTarget webTarget = client.target(new URI(TARGET_URI));
            Random rg = new Random();
            String randomElement;
            int listSize = list.size();
            randomElement = list.get(rg.nextInt(listSize));
            String message = getFeedData(randomElement);
            webTarget.path("twittersse").request().post(Entity.json(message));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void setQuery(String query) {
        Collection<Timer> timers = timerService.getAllTimers();
        for(Timer t : timers) {
             t.cancel();
        }
        list.clear();
        list.add(query);
        init();
    }


    private String getFeedData(String input) {
        StringBuilder sb = new StringBuilder();
        String searchURL = "http://search.twitter.com/search.json?q=" + input + "&rpp=5&include_entities=true" +
                "&with_twitter_user_id=true&result_type=mixed";
        try {
            URL twitter = new URL(searchURL);
            URLConnection yc;

            yc = twitter.openConnection();


            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            yc.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine);

            }
            in.close();

            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Error in getting data from the feeds ";
    }


}
