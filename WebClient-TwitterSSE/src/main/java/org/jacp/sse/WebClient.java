/*
 * Copyright (c) 2013, Andy Moncsek, inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 *    Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *    following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *    the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *    Neither the name of Andy Moncsek, inc. nor the names of its contributors may be used to endorse or
 *    promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jacp.sse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.glassfish.jersey.media.sse.EventSource;
import org.glassfish.jersey.media.sse.InboundEvent;

import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;

/**
 * Created with IntelliJ IDEA.
 * User: Andy Moncsek
 * Date: 08.03.13
 * Time: 09:30
 * A simple servlet that receives events and writes out the messages
 */
@WebServlet(name = "WebClient", urlPatterns = {"/WebClient"}, asyncSupported = true)
public class WebClient extends HttpServlet {

    private final static String TARGET_URI = "http://localhost:8080/"+SSEResource.URI+"/twittersse";

    @Override
    public void init() throws ServletException {

    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException    if a servlet-specific error occurs
     * @throws java.io.IOException if an I/O error occurs
     */
    @Override
    protected void service(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try {

            final AsyncContext asyncContext = request.startAsync();
            asyncContext.setTimeout(600000);
            asyncContext.addListener(new AsyncListener() {

                @Override
                public void onComplete(AsyncEvent event) throws IOException {
                }

                @Override
                public void onTimeout(AsyncEvent event) throws IOException {
                    System.out.println("Timeout" + event.toString());
                }

                @Override
                public void onError(AsyncEvent event) throws IOException {
                    System.out.println("Error" + event.toString());
                }

                @Override
                public void onStartAsync(AsyncEvent event) throws IOException {
                }
            });


            Thread t = new Thread(new AsyncRequestProcessor(asyncContext));
            t.start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * This class will create the EventSource
     * and when the SSE are received will print the data
     * from the Inbound events
     */
    class AsyncRequestProcessor implements Runnable {

        private final AsyncContext context;

        public AsyncRequestProcessor(AsyncContext context) {
            this.context = context;
        }

        @Override
        public void run() {
            Client client = ClientBuilder.newClient();
            context.getResponse().setContentType("text/html");
            final javax.ws.rs.client.WebTarget webTarget;
            try {
                final PrintWriter out = context.getResponse().getWriter();
                webTarget = client.target(new URI(TARGET_URI));
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Glassfish SSE TestClient</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>");
                out.println("All good tweets");
                out.println("</h1>");

                EventSource eventSource = new EventSource(webTarget) {
                    @Override
                    public void onEvent(InboundEvent inboundEvent) {
                        try {
                            //get the JSON data and parse it
                            System.out.println("EVENT: "+inboundEvent.getName());
                            JSONObject jsonObject = JSONObject.fromObject(inboundEvent.getData(String.class,
                                    MediaType.APPLICATION_JSON_TYPE));
                            JSONArray jsonArray = (((JSONArray) (jsonObject.get("results"))));
                            for (int i = 0; i < jsonArray.size(); i++) {
                                JSONObject o = ((JSONObject) jsonArray.get(i));
                                out.println(o.get("text"));
                                out.println("<br>");
                                out.println("Created at " + o.get("created_at"));
                                out.println("<br>");

                            }
                            out.println("</p>");
                            out.flush();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                };
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}