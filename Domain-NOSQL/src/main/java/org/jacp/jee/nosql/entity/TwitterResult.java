package org.jacp.jee.nosql.entity;

import org.eclipse.persistence.nosql.annotations.DataFormatType;
import org.eclipse.persistence.nosql.annotations.Field;
import org.eclipse.persistence.nosql.annotations.NoSql;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Andy Moncsek
 * Date: 11.03.13
 * Time: 09:13
 * To change this template use File | Settings | File Templates.
 */
@Entity
@NamedQueries({@NamedQuery(name = "TwitterResult.getAll", query = "Select t from TwitterResult t"), @NamedQuery(name = "TwitterResult.getAllByTweetQuery", query = "SELECT c FROM TwitterResult c WHERE c.tweetQuery = :query"),@NamedQuery(name = "TwitterResult.getAllTweetQueries", query = "Select DISTINCT t.tweetQuery from TwitterResult t")})
@NoSql(dataFormat = DataFormatType.MAPPED)
public class TwitterResult implements Serializable {
    @Id
    @GeneratedValue
    @Field(name = "_id")
    private String id;
    /* Optimistic locking is supported. */
    @Version
    private long version;

    @Embedded
    private TweetQuery tweetQuery;
    @Basic
    private String max_id;
    @ElementCollection
    private List<Tweet> results;


    public String getMax_id() {
        return max_id;
    }

    public void setMax_id(String max_id) {
        this.max_id = max_id;
    }

    public List<Tweet> getResults() {
        return results;
    }

    public void setResults(List<Tweet> results) {
        this.results = results;
    }


}
