package org.jacp.annotations;

/**
 * Created with IntelliJ IDEA.
 * User: Andy Moncsek
 * Date: 13.06.13
 * Time: 15:03
 * To change this template use File | Settings | File Templates.
 */

import javax.inject.Qualifier;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This is the CDI event classifier
 *
 * @author Bruno Borges <bruno.borges at oracle.com>
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER, ElementType.TYPE})
public @interface JMSMessage {
}
