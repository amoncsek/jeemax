package org.jacp.endpoint;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 14.06.13
 * Time: 08:33
 * To change this template use File | Settings | File Templates.
 */
/**
 * Copyright © 2013, 2013, Oracle and/or its affiliates. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.jacp.annotations.JMSMessage;
import org.jacp.beans.jms.QueueSenderSessionBean;
import org.jacp.decoder.MessageDecoder;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.websocket.server.ServerEndpoint;

import org.jacp.ejb.ChatUserBean;
import org.jacp.encoder.ChatMessageEncoder;
import org.jacp.encoder.UserAddMessageEncoder;
import org.jacp.jee.entity.ChatUser;
import org.jacp.jee.messages.ChatMessage;
import org.jacp.jee.messages.LoginMessage;
import org.jacp.jee.messages.UserAddMessage;

/**
 * This is the WebSocket server endpoint. It listens to CDI events classified
 * with
 * <code>@WSJMSMessage</code> at <code>onJMSMessage</code> and sends the payload
 * to all client Sessions
 * @author Bruno Borges <bruno.borges at oracle.com>
 */
@ServerEndpoint(value = "/jmschat", decoders = {MessageDecoder.class},encoders = {UserAddMessageEncoder.class,ChatMessageEncoder.class, UserAddMessageEncoder.class  })
public class WebSocketEndpoint implements Serializable {

    @Inject
    private QueueSenderSessionBean senderBean;
    @Inject
    private ChatUserBean chatBean;

    private static final Set<Session> sessions = Collections.synchronizedSet(new HashSet<Session>());
    final static Logger logger = Logger.getLogger("application");
    @OnOpen
    public void onOpen(final Session session) {
        try {
            logger.info("############Someone connected..." + session.getId() + "  socket: " + chatBean);
            sessions.add(session);

            if (senderBean == null) {
                Logger.getLogger(WebSocketEndpoint.class.getName()).log(Level.INFO, "senderBean is null");
            }
        } catch (Exception ex) {
            Logger.getLogger(WebSocketEndpoint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }



    @OnMessage
    public boolean handleChatMessage(org.jacp.jee.messages.Message cm, Session session) {
        logger.info("############message received");
        switch (cm.getType()){
            case MESSAGE:
                logger.info("############message received");
                ChatMessage message = (ChatMessage) cm;
                message.setSourceSessionId(session.getId());
                senderBean.sendMessage(cm);
                break;
            case LOGIN:
                logger.info("############login received");
                LoginMessage lrm = (LoginMessage) cm;
                lrm.setSourceSessionId(session.getId());
                chatBean.saveNewUser(new ChatUser(lrm.getUserName(),session.getId()));
                senderBean.sendMessage(cm);
                break;
        }
        return true;
    }

    private void broadcastMessage(ChatMessage cm) {

        for (Session s : sessions) {
            logger.info("send message : " + cm.getMessage());
            try {
                s.getBasicRemote().sendObject(cm);
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (EncodeException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

        }
    }

    private void broadcastNewUser(UserAddMessage newUser) {
        try {
            final List<ChatUser> all = chatBean.getAllUsers();
            for (Session s : sessions) {
                // send existing users to new user
                if (s.getId().equals(newUser.getSourceSessionId())) {
                    for(ChatUser user : all){
                        s.getBasicRemote().sendObject(new UserAddMessage(user.getName()));
                    }
                }else {
                    // send new user to existing
                    s.getBasicRemote().sendObject(newUser);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (EncodeException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @OnClose
    public void onClose(final Session session) {
        try {
            session.getBasicRemote().sendText("WebSocket Session closed");
            sessions.remove(session);
        } catch (Exception ex) {
            Logger.getLogger(WebSocketEndpoint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onJMSMessage(@Observes @JMSMessage Message msg) {
        Logger.getLogger(WebSocketEndpoint.class.getName()).log(Level.INFO, "Got JMS Message at WebSocket!");

        try {
            Serializable message = msg.getBody(Serializable.class);
            Logger.getLogger(WebSocketEndpoint.class.getName()).log(Level.INFO, ":::"+message);
             if(message instanceof LoginMessage){
                 UserAddMessage m =    new UserAddMessage(LoginMessage.class.cast(message).getUserName());
                 m.setSourceSessionId(LoginMessage.class.cast(message).getSourceSessionId());
                 broadcastNewUser(m);
             } else if(message instanceof ChatMessage) {
                  broadcastMessage(ChatMessage.class.cast(message));
             }

        } catch (JMSException ex) {
            Logger.getLogger(WebSocketEndpoint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}