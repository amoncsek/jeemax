package org.jacp.beans.jms;

/**
 * Created with IntelliJ IDEA.
 * User: Andy Moncsek
 * Date: 13.06.13
 * Time: 15:04
 * To change this template use File | Settings | File Templates.
 */

import org.jacp.annotations.JMSMessage;

import javax.ejb.MessageDriven;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactoryDefinition;
import javax.jms.JMSDestinationDefinition;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 * This MDB will fire CDI events with the JMS payload, classified as
 * <code>@WSJMSMessage</code>
 *
 * @author Bruno Borges <bruno.borges at oracle.com>
 */

@MessageDriven(mappedName = "jms/SimpleQueue")
public class WebSocketMDB implements MessageListener {

    @Inject
    @JMSMessage
    Event<Message> jmsEvent;

    @Override
    public void onMessage(Message msg) {
        jmsEvent.fire(msg);
    }
}