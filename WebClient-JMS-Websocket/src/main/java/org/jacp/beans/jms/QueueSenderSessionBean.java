package org.jacp.beans.jms;

/**
 * Created with IntelliJ IDEA.
 * User: Andy Moncsek
 * Date: 13.06.13
 * Time: 14:55
 * To change this template use File | Settings | File Templates.
 */

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;
import java.io.Serializable;

/**
 * This is the SessionBean used by the WebSocket server endpoint to dispatch
 * incoming messages to the JMS Queue
 *
 * @author Bruno Borges <bruno.borges at oracle.com>
 */
@Stateless
public class QueueSenderSessionBean {

    @Resource(mappedName = "jms/SimpleQueue")
    private Queue myQueue;
    @Inject @JMSConnectionFactory(
            "jms/QueueConnectionFactory")
    private JMSContext jmsContext;

    public void sendMessage(Serializable message) {
        jmsContext.createProducer().send(myQueue, message);
    }
}
