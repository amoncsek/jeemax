package org.jacp.encoder;

import org.apache.commons.lang.SerializationUtils;
import org.jacp.jee.messages.UserAddMessage;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import java.nio.ByteBuffer;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 27.02.13
 * Time: 12:51
 * To change this template use File | Settings | File Templates.
 */
public class UserAddMessageEncoder implements Encoder.Binary<UserAddMessage> {
    @Override
    public ByteBuffer encode(UserAddMessage userAddMessage) throws EncodeException {
        return ByteBuffer.wrap(SerializationUtils.serialize(userAddMessage));
    }

    @Override
    public void init(EndpointConfig endpointConfig) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void destroy() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
