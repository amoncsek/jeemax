<%-- 
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2012 Oracle and/or its affiliates. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License.  You can
 * obtain a copy of the License at
 * https://glassfish.dev.java.net/public/CDDL+GPL_1_1.html
 * or packager/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at packager/legal/LICENSE.txt.
 *
 * GPL Classpath Exception:
 * Oracle designates this particular file as subject to the "Classpath"
 * exception as provided by Oracle in the GPL Version 2 section of the License
 * file that accompanied this code.
 *
 * Modifications:
 * If applicable, add the following below the License Header, with the fields
 * enclosed by brackets [] replaced by your own identifying information:
 * "Portions Copyright [year] [name of copyright owner]"
 *
 * Contributor(s):
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
--%>
<%@page import="java.util.Enumeration"%>
<%@ page import="java.net.InetAddress" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Hello WebSocket</title>

    <script language="javascript" type="text/javascript">
        var wsUri = "ws://localhost:8080/WebClient-Websocket/echo";
        var websocket = new WebSocket(wsUri);
        websocket.onopen = function (evt) {
            onOpen(evt)
        };
        websocket.onmessage = function (evt) {
            onMessage(evt)
        };
        websocket.onerror = function (evt) {
            onError(evt)
        };

        function init() {
            output = document.getElementById("output");
        }

        function say_hello() {
            websocket.send(nameField.value);
            writeToScreen("SENT: " + nameField.value);
        }

        function onOpen(evt) {
            writeToScreen('<span style="color: red;">CONNECTED:</span>: ');
        }

        function onMessage(evt) {
            writeToScreen('<span style="color: red;">RECEIVED:</span>: ' + evt.data);
        }

        function onError(evt) {
            writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
        }

        function writeToScreen(message) {
            var pre = document.createElement("p");
            pre.style.wordWrap = "break-word";
            pre.innerHTML = message;
            output.appendChild(pre);
        }

        window.addEventListener("load", init, false);
    </script>
</head>
<body>
<%String ip = "";
    InetAddress inetAddress = InetAddress.getLocalHost();
    ip = inetAddress.getHostAddress();
    out.println("Server Host Name :: "+inetAddress.getHostName());%><br>
<%out.println("Server IP Address :: "+ip);%>
<%
    if (request.getParameter("sessionName") != null && request.getParameter("sessionValue") != null) {
        HttpSession httpSession = request.getSession();
        httpSession.setAttribute(request.getParameter("sessionName"), request.getParameter("sessionValue"));
    }
%>

<form action="index.jsp" method="POST">

    your session name : <input type="text" name="sessionName" /> <br />
    your session value : <input type="text" name="sessionValue" /> <br />
    <input type="submit" />

</form>

<h1>Your Sessions</h1>

<%
    Enumeration sessionNames = session.getAttributeNames();
    while (sessionNames.hasMoreElements()) {
        String mySessionName = sessionNames.nextElement().toString();
        String mySession = request.getSession().getAttribute(mySessionName).toString();
        out.print(mySessionName+ " --> "+mySession + " <br />");
    }
%>

<div style="text-align: center;">
    <form action="">
        <input onclick="say_hello()" value="Say Hello" type="button">
        <input id="nameField" name="name" value="WebSocket" type="text"><br>
    </form>
</div>
<div id="output"></div>
</body>
</html>
