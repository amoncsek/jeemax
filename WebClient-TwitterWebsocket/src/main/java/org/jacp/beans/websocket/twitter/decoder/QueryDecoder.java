package org.jacp.beans.websocket.twitter.decoder;

import org.apache.commons.lang.SerializationUtils;
import org.jacp.jee.entity.Query;
import org.jacp.jee.messages.Message;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;
import java.nio.ByteBuffer;

/**
 * Created with IntelliJ IDEA.
 * User: ady
 * Date: 27.02.13
 * Time: 15:32
 * To change this template use File | Settings | File Templates.
 */
public class QueryDecoder implements Decoder.Binary<Query> {

    @Override
    public Query decode(ByteBuffer byteBuffer) throws DecodeException {
        return (Query) SerializationUtils.deserialize(byteBuffer.array());
    }

    @Override
    public boolean willDecode(ByteBuffer byteBuffer) {
        Object message = SerializationUtils.deserialize(byteBuffer.array());
        if (message == null) return false;
        return message instanceof Query;
    }

    @Override
    public void init(EndpointConfig endpointConfig) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void destroy() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}


