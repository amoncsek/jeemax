package org.jacp.beans.websocket.twitter;


import org.jacp.beans.ejb.TwitterRepositoryBean;
import org.jacp.beans.websocket.twitter.decoder.QueryDecoder;
import org.jacp.beans.websocket.twitter.encoder.TwitterResultEncoder;
import org.jacp.jee.entity.Query;
import org.jacp.jee.entity.TwitterResult;

import javax.inject.Inject;
import javax.websocket.EncodeException;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Andy Moncsek
 * Date: 18.04.13
 * Time: 15:57
 * To change this template use File | Settings | File Templates.
 */
@ServerEndpoint(value = "/twitter", decoders = {QueryDecoder.class},encoders = {TwitterResultEncoder.class})
public class TwitterEndpoint {
    final static Logger logger = Logger.getLogger("application");

    @Inject
    private TwitterRepositoryBean twitterRepo;

    @OnOpen
    public void init(Session s) {
        logger.info("############Someone connected..." + s.getId() + "  socket: " + twitterRepo);
    }

    @OnMessage
    public void handleChatMessage(Query query, Session session) {
        TwitterResult result = twitterRepo.getTwitterDataByQuery(query);
        broadcastMessage(result,session);

    }

    private void broadcastMessage(TwitterResult result, Session session) {
        for (Session s : session.getOpenSessions()) {
            try {
                s.getBasicRemote().sendObject(result);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (EncodeException e) {
                e.printStackTrace();
            }

        }
    }
}
